// uart.h
// Kyle Dillon 4/30/14
// Init, read, write functions for using UART1 on Teensy 2.0

void init_uart();
void uart_putc(char data);
void uart_puts(char *str);
uint8_t uart_getc(void);
