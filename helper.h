//helper.h
//Kyle Dillon 11/7/13
// Simple multi-use-case function library.
#include <avr/io.h>

void set_bit(uint8_t bit, uint8_t *byte);
void clear_bit(uint8_t bit, uint8_t *byte);
void toggle_bit(uint8_t bit, uint8_t *byte);
uint8_t check_bit(uint8_t bit, uint8_t byte);
uint16_t concatenate_ints(uint16_t x, uint16_t y);
void reverse_string(char s[]);
