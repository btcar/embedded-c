// adc.h
// Kyle Dillon 4/30/14
// Simple init and read ADC library for Teensy 2.0.
#include <avr/io.h>

void init_adc(void);
uint16_t read_adc(uint8_t adc);
